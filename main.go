package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"os"
	"os/exec"
	"syscall"
)

// Very simple program that just wraps arbitrary command
// and runs it as random user with uid between 2^10 and 2^16
func main() {
	var id uint64
	for id == 0 {
		b := make([]byte, 2)
		_, err := rand.Read(b[:1])
		if err != nil {
			panic(err)
		}
		b[0] = b[0] << 2
		id = big.NewInt(0).SetBytes(b).Uint64()
	}
	cmd := exec.Command(os.Args[1], os.Args[2:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(id), Gid: uint32(id)}
	if err := cmd.Run(); err != nil {
		if exerr, ok := err.(*exec.ExitError); ok {
			if status, ok := exerr.Sys().(syscall.WaitStatus); ok {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(status.ExitStatus())
			}
		}
		panic(err)
	}
}
